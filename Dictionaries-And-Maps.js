function processData(input) {

    var input = input.split('\n')
    var n = Number(input[0])
    var phoneBook = []

    // To determine the name and the phone number
    for (var i = 0; i < n; i++) {
        var line = input[i+1].split(' ')
        phoneBook[line[0]] = line[1]
    }

    for (var i = n+1; i < input.length; i++) {
        var num = (phoneBook[input[i]])

        if (!num) {
            console.log('Not found')
        } else {
            console.log(input[i] + '=' + num)
        }
    }
}