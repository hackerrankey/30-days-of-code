function processData(input) {

    var str = input.split('\n')
    var length = Number(str[0]) + 1
    var even = ''
    var odd = ''

    for (var i = 1; i < length; i++) {
        var strArr = String(str[i]).split('')
        var lengthArr = strArr.length

        for (var j = 0; j < lengthArr; j++) {
            if (j == 0 || j % 2 == 0) {
                even = even + strArr[j]
            } else if (j % 2 != 0) {
                odd = odd + strArr[j]
            }
        }

        even = even + ' '
        odd = odd + ' '
    }

    even = even.split(' ')
    odd = odd.split(' ')

    for (var k = 0; k < Number(str[0]); k++) {
        console.log(`${even[k]} ${odd[k]}`)
    }
}